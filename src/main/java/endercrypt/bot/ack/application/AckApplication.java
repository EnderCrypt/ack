package endercrypt.bot.ack.application;


import endercrypt.bot.ack.application.arguments.Arguments;
import endercrypt.bot.ack.application.configuration.Configurations;
import endercrypt.bot.ack.application.modules.Modules;
import endercrypt.library.framework.Application;


public class AckApplication extends Application
{
	public static void main(String[] args)
	{
		Application.launch(AckApplication.class, args);
	}
	
	@Override
	public Arguments arguments()
	{
		return (Arguments) super.arguments();
	}
	
	@Override
	public Configurations configurations()
	{
		return (Configurations) super.configurations();
	}
	
	@Override
	public Modules modules()
	{
		return (Modules) super.modules();
	}
}
