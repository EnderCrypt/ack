package endercrypt.bot.ack.application.configuration;


import endercrypt.bot.ack.application.configuration.keys.ConfigurationSecrets;
import endercrypt.library.commons.data.DataSource;
import endercrypt.library.framework.systems.configuration.Configuration;
import endercrypt.library.framework.systems.configuration.StandardConfigurations;


public interface Configurations extends StandardConfigurations
{
	@Configuration(location = DataSource.DISK, path = "etc/secrets.yaml")
	public ConfigurationSecrets secrets();
}
