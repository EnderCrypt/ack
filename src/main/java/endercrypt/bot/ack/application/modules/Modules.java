package endercrypt.bot.ack.application.modules;


import endercrypt.bot.ack.modules.command.CommandModule;
import endercrypt.bot.ack.modules.discord.DiscordModule;
import endercrypt.bot.ack.modules.terminal.TerminalModule;
import endercrypt.bot.ack.modules.variable.VariableModule;
import endercrypt.library.framework.systems.modules.StandardModules;


public interface Modules extends StandardModules
{
	public DiscordModule discord();
	
	public CommandModule command();
	
	public TerminalModule terminal();
	
	public VariableModule variable();
}
