package endercrypt.bot.ack.application.modules;


import endercrypt.bot.ack.application.AckApplication;
import endercrypt.library.framework.systems.modules.CoreModule;


public class AckModule extends CoreModule<AckApplication>
{
	public AckModule(AckApplication application)
	{
		super(application);
	}
}
