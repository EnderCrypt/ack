package endercrypt.bot.ack.interaction.message;


import endercrypt.bot.ack.interaction.SystemAckEntity;
import endercrypt.bot.ack.interaction.channel.SystemAckChannel;
import endercrypt.bot.ack.interaction.user.AckUser;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SystemAckMesssage extends SystemAckEntity implements AckMessage
{
	public static SystemAckMesssage from(SystemAckChannel channel, String text)
	{
		return new SystemAckMesssage(channel, text);
	}
	
	public static SystemAckMesssage poll()
	{
		return SystemAckChannel.instance.poll();
	}
	
	private final SystemAckChannel channel;
	private final String text;
	
	@Override
	public AckUser getUser()
	{
		return null;
	}
}
