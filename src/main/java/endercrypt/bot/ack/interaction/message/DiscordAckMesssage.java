package endercrypt.bot.ack.interaction.message;


import endercrypt.bot.ack.interaction.DiscordAckEntity;
import endercrypt.bot.ack.interaction.channel.DiscordAckChannel;
import endercrypt.bot.ack.interaction.user.DiscordAckUser;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;


@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DiscordAckMesssage extends DiscordAckEntity implements AckMessage
{
	public static DiscordAckMesssage from(MessageReceivedEvent event)
	{
		return from(event.getMessage());
	}
	
	public static DiscordAckMesssage from(Message message)
	{
		DiscordAckUser user = DiscordAckUser.from(message.getAuthor());
		DiscordAckChannel channel = DiscordAckChannel.from(message.getChannel());
		String text = message.getContentRaw();
		return new DiscordAckMesssage(user, channel, text);
	}
	
	private final DiscordAckUser user;
	private final DiscordAckChannel channel;
	private final String text;
}
