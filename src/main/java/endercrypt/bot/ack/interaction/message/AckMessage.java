package endercrypt.bot.ack.interaction.message;


import endercrypt.bot.ack.interaction.AckEntity;
import endercrypt.bot.ack.interaction.channel.AckChannel;
import endercrypt.bot.ack.interaction.user.AckUser;
import endercrypt.bot.ack.sendable.Sendable;


public interface AckMessage extends AckEntity
{
	public AckUser getUser();
	
	public AckChannel getChannel();
	
	public String getText();
	
	default void reply(Sendable sendable)
	{
		getChannel().send(sendable);
	}
}
