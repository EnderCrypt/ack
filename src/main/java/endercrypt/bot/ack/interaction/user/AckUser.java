package endercrypt.bot.ack.interaction.user;


import endercrypt.bot.ack.interaction.AckEntity;


public interface AckUser extends AckEntity
{
	public abstract String getName();
}
