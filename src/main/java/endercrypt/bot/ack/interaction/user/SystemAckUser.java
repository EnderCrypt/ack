package endercrypt.bot.ack.interaction.user;


import endercrypt.bot.ack.interaction.SystemAckEntity;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SystemAckUser extends SystemAckEntity implements AckUser
{
	private static final SystemAckUser instance = new SystemAckUser();
	
	public static SystemAckUser from()
	{
		return instance;
	}
	
	@Override
	public String getName()
	{
		return "<SYSTEM>";
	}
}
