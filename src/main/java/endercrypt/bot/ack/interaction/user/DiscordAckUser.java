package endercrypt.bot.ack.interaction.user;


import endercrypt.bot.ack.interaction.DiscordAckEntity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.entities.User;


@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DiscordAckUser extends DiscordAckEntity implements AckUser
{
	public static DiscordAckUser from(User user)
	{
		return new DiscordAckUser(user);
	}
	
	@Getter
	private final User user;
	
	@Override
	public String getName()
	{
		return user.getName();
	}
}
