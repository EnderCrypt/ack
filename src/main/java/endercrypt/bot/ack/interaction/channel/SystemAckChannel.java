package endercrypt.bot.ack.interaction.channel;


import endercrypt.bot.ack.interaction.DiscordAckEntity;
import endercrypt.bot.ack.interaction.message.SystemAckMesssage;
import endercrypt.bot.ack.modules.command.CommandModule;
import endercrypt.bot.ack.sendable.Sendable;
import endercrypt.bot.ack.sendable.sender.SendableSender;

import java.io.PrintStream;
import java.util.Scanner;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SystemAckChannel extends DiscordAckEntity implements AckChannel
{
	public static SystemAckChannel instance = new SystemAckChannel();
	
	private final Scanner in = new Scanner(System.in);
	private final PrintStream out = System.out;
	
	public SystemAckMesssage poll()
	{
		String text = in.nextLine();
		return SystemAckMesssage.from(this, CommandModule.callWord + ", " + text);
	}
	
	private final SendableSender<Sendable.ToSystem> sender = new SendableSender<>(Sendable.ToSystem.class, this::send);
	
	private void send(Sendable.ToSystem send)
	{
		send.toSystem(getOut());
	}
}
