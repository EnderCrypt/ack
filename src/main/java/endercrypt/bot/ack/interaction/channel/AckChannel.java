package endercrypt.bot.ack.interaction.channel;


import endercrypt.bot.ack.interaction.AckEntity;
import endercrypt.bot.ack.sendable.Sendable;
import endercrypt.bot.ack.sendable.sender.SendableSender;


public interface AckChannel extends AckEntity
{
	public SendableSender<? extends Sendable> getSender();
	
	default void send(Sendable sendable)
	{
		getSender().send(sendable);
	}
}
