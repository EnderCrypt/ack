package endercrypt.bot.ack.interaction.channel;


import endercrypt.bot.ack.interaction.DiscordAckEntity;
import endercrypt.bot.ack.sendable.Sendable;
import endercrypt.bot.ack.sendable.sender.SendableSender;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;


@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DiscordAckChannel extends DiscordAckEntity implements AckChannel
{
	public static DiscordAckChannel from(MessageChannel channel)
	{
		return new DiscordAckChannel(channel);
	}
	
	private final MessageChannel channel;
	
	private final SendableSender<Sendable.ToDiscord> sender = new SendableSender<>(Sendable.ToDiscord.class, this::send);
	
	private void send(Sendable.ToDiscord send)
	{
		send.toDiscord(getChannel());
	}
}
