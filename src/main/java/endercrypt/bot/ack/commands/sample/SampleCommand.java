package endercrypt.bot.ack.commands.sample;


import endercrypt.bot.ack.application.AckApplication;
import endercrypt.bot.ack.interaction.message.AckMessage;
import endercrypt.bot.ack.modules.command.CommandCollection;
import endercrypt.bot.ack.sendable.types.SendableFile;
import endercrypt.bot.ack.sendable.types.SendableText;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;


@CommandSignature("sample")
public class SampleCommand extends CommandCollection
{
	public SampleCommand(AckApplication application)
	{
		super(application);
	}
	
	@CommandSignature("text")
	public void test(@Include AckMessage message)
	{
		message.reply(new SendableText("lol: " + message));
	}
	
	@CommandSignature("file")
	public void file(@Include AckMessage message)
	{
		message.reply(new SendableFile("file.dat", new byte[] { 1, 4, 46 }));
	}
	
	@CommandSignature("file test")
	public void fileCheck(@Include AckMessage message)
	{
		StringBuilder buffer = new StringBuilder();
		buffer.append("you");
		if (message.getChannel().getSender().isSupported(SendableFile.class) == false)
		{
			buffer.append(" do not");
		}
		buffer.append(" support file uploads");
		message.reply(new SendableText(buffer));
	}
}
