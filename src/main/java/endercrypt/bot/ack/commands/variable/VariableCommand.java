package endercrypt.bot.ack.commands.variable;


import endercrypt.bot.ack.application.AckApplication;
import endercrypt.bot.ack.interaction.message.AckMessage;
import endercrypt.bot.ack.modules.command.CommandCollection;
import endercrypt.bot.ack.modules.variable.AckStorable;
import endercrypt.bot.ack.modules.variable.AckVariable;
import endercrypt.bot.ack.sendable.Viewable;
import endercrypt.bot.ack.sendable.types.SendableText;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;

import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;


@CommandSignature("variable")
public class VariableCommand extends CommandCollection
{
	public VariableCommand(AckApplication application)
	{
		super(application);
	}
	
	@CommandSignature("list")
	public void list(@Include AckMessage message)
	{
		StringBuilder buffer = new StringBuilder();
		
		List<AckVariable> variables = application.modules().variable().getAll();
		
		if (variables.isEmpty())
		{
			buffer.append("No variables present.");
		}
		else
		{
			buffer
				.append("Variables: (")
				.append(variables.size())
				.append(")\n");
		}
		
		String info = variables.stream()
			.map(Viewable::getInfo)
			.collect(Collectors.joining("\n"));
		buffer.append(info);
		
		message.reply(new SendableText(buffer));
	}
	
	@CommandSignature("test")
	public void test(@Include AckMessage message)
	{
		AckVariable variable = application.modules().variable().push("test", new Test());
		message.reply(new SendableText("Created test variable: " + variable.getName()));
	}
	
	@SuppressWarnings("serial")
	@Getter
	public static class Test implements AckStorable
	{
		@Override
		public String getInfo()
		{
			return "-> " + Math.random();
		}
	}
}
