package endercrypt.bot.ack.sendable.types;


import endercrypt.bot.ack.sendable.Sendable;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.utils.FileUpload;


@Getter
@RequiredArgsConstructor
public class SendableFile implements Sendable, Sendable.ToDiscord
{
	private final String fileName;
	private final byte[] bytes;
	
	@Override
	public void toDiscord(MessageChannel channel)
	{
		FileUpload upload = FileUpload.fromData(getBytes(), getFileName());
		channel.sendFiles(upload).complete();
	}
	
	@Override
	public String getFallbackDescription()
	{
		return "Filename: " + getFileName();
	}
}
