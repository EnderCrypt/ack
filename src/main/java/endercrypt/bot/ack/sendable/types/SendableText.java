package endercrypt.bot.ack.sendable.types;


import endercrypt.bot.ack.sendable.Sendable;

import java.io.PrintStream;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;


@Getter
@RequiredArgsConstructor
public class SendableText implements Sendable, Sendable.ToSystem, Sendable.ToDiscord
{
	private final CharSequence text;
	
	@Override
	public void toSystem(PrintStream stream)
	{
		stream.println(getText());
	}
	
	@Override
	public void toDiscord(MessageChannel channel)
	{
		channel.sendMessage(getText()).complete();
	}
	
	@Override
	public String getFallbackDescription()
	{
		throw new UnsupportedOperationException("platform did not support sending text, so attempting to send text wouldnt work");
	}
}
