package endercrypt.bot.ack.sendable;


import java.util.Optional;


public interface Viewable
{
	public String getInfo();
	
	public static String getNullableInfo(Viewable viewable, String nullMessage)
	{
		return Optional.ofNullable(viewable)
			.map(Viewable::getInfo)
			.orElse(nullMessage);
	}
}
