package endercrypt.bot.ack.sendable.sender;


import endercrypt.bot.ack.sendable.Sendable;
import endercrypt.bot.ack.sendable.types.SendableText;

import java.util.function.Consumer;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;


@Log4j2
@RequiredArgsConstructor
@Getter
public class SendableSender<T extends Sendable>
{
	private final Class<T> targetClass;
	private final Consumer<T> sender;
	
	public void send(Sendable sendable)
	{
		try
		{
			sendOrFail(sendable);
		}
		catch (SendableNotSupported e1)
		{
			try
			{
				fallbackSend(sendable);
			}
			catch (SendableNotSupported e2)
			{
				logger.error("Failed to send fallback sendable for " + sendable.getClass().getSimpleName(), e2);
			}
		}
	}
	
	public boolean optionalSend(Sendable sendable)
	{
		if (getTargetClass().isInstance(sendable))
		{
			T instance = getTargetClass().cast(sendable);
			getSender().accept(instance);
			return true;
		}
		return false;
	}
	
	public void sendOrFail(Sendable sendable) throws SendableNotSupported
	{
		if (optionalSend(sendable) == false)
		{
			throw new SendableNotSupported("Failed to send " + sendable + " as lacks " + getTargetClass());
		}
	}
	
	private void fallbackSend(Sendable sendable) throws SendableNotSupported
	{
		String fallbackDescription = sendable.getFallbackDescription();
		Sendable fallbackSendable = new SendableText("<" + fallbackDescription + ">");
		sendOrFail(fallbackSendable);
	}
	
	public boolean isSupported(Class<? extends Sendable> sendableClass)
	{
		return targetClass.isAssignableFrom(sendableClass);
	}
}
