package endercrypt.bot.ack.sendable.sender;

@SuppressWarnings("serial")
public class SendableNotSupported extends Exception
{
	public SendableNotSupported(String message)
	{
		super(message);
	}
	
	public SendableNotSupported(String message, Throwable cause)
	{
		super(message, cause);
	}
}
