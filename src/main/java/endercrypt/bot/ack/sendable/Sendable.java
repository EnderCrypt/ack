package endercrypt.bot.ack.sendable;


import java.io.PrintStream;

import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;


public interface Sendable
{
	public interface ToSystem extends Sendable
	{
		public void toSystem(PrintStream stream);
	}
	
	public interface ToDiscord extends Sendable
	{
		public void toDiscord(MessageChannel channel);
	}
	
	public String getFallbackDescription();
}
