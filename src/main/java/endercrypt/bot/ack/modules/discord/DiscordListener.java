package endercrypt.bot.ack.modules.discord;


import endercrypt.bot.ack.application.AckApplication;
import endercrypt.bot.ack.interaction.message.DiscordAckMesssage;

import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;


@RequiredArgsConstructor
public class DiscordListener extends ListenerAdapter
{
	private final AckApplication application;
	
	@Override
	public void onMessageReceived(MessageReceivedEvent event)
	{
		DiscordAckMesssage message = DiscordAckMesssage.from(event);
		application.modules().command().execute(message);
	}
}
