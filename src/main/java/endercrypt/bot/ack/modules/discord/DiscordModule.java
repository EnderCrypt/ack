package endercrypt.bot.ack.modules.discord;


import endercrypt.bot.ack.application.AckApplication;
import endercrypt.bot.ack.application.modules.AckModule;

import java.util.List;

import lombok.extern.log4j.Log4j2;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;


@Log4j2
public class DiscordModule extends AckModule
{
	public static List<GatewayIntent> intents = List.of(
		GatewayIntent.GUILD_MESSAGES,
		GatewayIntent.GUILD_PRESENCES,
		GatewayIntent.GUILD_MEMBERS,
		GatewayIntent.MESSAGE_CONTENT);
	
	private JDA jda;
	
	public DiscordModule(AckApplication application)
	{
		super(application);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		String key = application.configurations().secrets().getDiscord();
		JDABuilder builder = JDABuilder.create(key, intents);
		builder.addEventListeners(new DiscordListener(application));
		jda = builder.build();
		jda.awaitReady();
		logger.info("Connected to discord " + jda + " (status: " + jda.getStatus() + ")");
	}
}
