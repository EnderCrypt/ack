package endercrypt.bot.ack.modules.command;


import endercrypt.bot.ack.application.AckApplication;
import endercrypt.bot.ack.application.modules.AckModule;
import endercrypt.bot.ack.interaction.message.AckMessage;
import endercrypt.bot.ack.sendable.types.SendableText;
import endercrypt.library.intellicommand.IntelliCommand;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.exception.execution.CommandArgumentMismatchException;
import endercrypt.library.intellicommand.exception.execution.CommandNotFoundException;
import endercrypt.library.intellicommand.exception.execution.MalformedCommandException;
import endercrypt.library.intellicommand.exception.execution.UnderlyingCommandException;

import java.util.Optional;
import java.util.stream.Collectors;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class CommandModule extends AckModule
{
	public static final String callWord = "Ack";
	
	private final IntelliCommand intelliCommand = new IntelliCommand();
	
	public CommandModule(AckApplication application)
	{
		super(application);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		CommandLoader.collectDefault(application).into(intelliCommand);
	}
	
	public boolean execute(AckMessage message)
	{
		String commandText = findCommand(message.getText()).orElse(null);
		if (commandText == null)
		{
			return false;
		}
		
		Bundle bundle = Bundle.createWith()
			.instance("message", message)
			.getBundle();
		
		try
		{
			intelliCommand.execute(commandText, bundle);
		}
		catch (CommandArgumentMismatchException e)
		{
			String info = e.getCommands()
				.stream()
				.map(c -> c.getPrimarySignature().asText())
				.collect(Collectors.joining("\n\t - "));
			
			message.reply(new SendableText("did you mean?\n\t - " + info));
		}
		catch (CommandNotFoundException e)
		{
			message.reply(new SendableText("Command not found"));
		}
		catch (MalformedCommandException e)
		{
			message.reply(new SendableText(e.getMessage()));
		}
		catch (UnderlyingCommandException e)
		{
			message.reply(new SendableText("Error executing command!"));
			logger.error("failed to execute command: " + e.getCommand().getPrimarySignature().asText(), e);
		}
		return true;
	}
	
	private static Optional<String> findCommand(String text)
	{
		text = text.strip();
		int ackMentionIndex = text.toLowerCase().indexOf(callWord.toLowerCase());
		if (ackMentionIndex != 0)
		{
			return Optional.empty();
		}
		String leftover = text.substring(callWord.length());
		while (true)
		{
			if (leftover.length() == 0)
			{
				return Optional.empty();
			}
			if (Character.isAlphabetic(leftover.charAt(0)) == false)
			{
				leftover = leftover.substring(1);
			}
			else
			{
				return Optional.of(leftover);
			}
		}
	}
}
