package endercrypt.bot.ack.modules.command;


import endercrypt.bot.ack.application.AckApplication;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class CommandCollection
{
	protected final AckApplication application;
}
