package endercrypt.bot.ack.modules.command;

@SuppressWarnings("serial")
public class AckCommandException extends RuntimeException
{
	public AckCommandException(String message)
	{
		super(message);
	}
	
	public AckCommandException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
