package endercrypt.bot.ack.modules.command;


import endercrypt.bot.ack.application.AckApplication;
import endercrypt.library.commons.misc.Ender;
import endercrypt.library.intellicommand.IntelliCommand;
import endercrypt.library.intellicommand.command.Command;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.reflections.Reflections;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class CommandLoader
{
	public static CommandLoader collectDefault(AckApplication application)
	{
		String targetPackage = application.getInformation().getPackage() + ".commands";
		return new CommandLoader(application, targetPackage);
	}
	
	private final AckApplication application;
	private final String targetPackage;
	private final Reflections reflections;
	
	public CommandLoader(AckApplication application, String targetPackage)
	{
		this.application = application;
		this.targetPackage = targetPackage;
		this.reflections = new Reflections(targetPackage);
	}
	
	public Collection<? extends CommandCollection> collect()
	{
		return reflections.getSubTypesOf(CommandCollection.class).stream()
			.map(this::constructCommandClass)
			.collect(Collectors.toList());
	}
	
	private <T extends CommandCollection> T constructCommandClass(Class<T> commandCollectionClass)
	{
		try
		{
			Constructor<T> constructor = commandCollectionClass.getConstructor(AckApplication.class);
			return constructor.newInstance(application);
		}
		catch (ReflectiveOperationException e)
		{
			throw new AckCommandException("Failed to construct " + commandCollectionClass.getName(), e);
		}
	}
	
	public void into(IntelliCommand intelliCommand)
	{
		for (CommandCollection commandCollection : collect())
		{
			Class<? extends CommandCollection> commandCollectionClass = commandCollection.getClass();
			
			List<Command> commands = intelliCommand.commands().register(commandCollection);
			if (commands.isEmpty())
			{
				throw new AckCommandException(commandCollectionClass.getClass() + " has no commands");
			}
			
			String fullName = commandCollection.getClass().getName();
			String relativeName = Ender.text.removeBeginning(fullName, targetPackage)
				.orElseThrow(() -> new IllegalArgumentException("command " + commandCollectionClass.getSimpleName() + " is in package " + commandCollectionClass.getPackageName() + " but was expected to be in " + targetPackage));
			logger.info("Registered " + commands.size() + " commands from " + relativeName);
		}
	}
}
