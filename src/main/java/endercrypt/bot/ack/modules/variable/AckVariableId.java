package endercrypt.bot.ack.modules.variable;


import endercrypt.library.commons.misc.Ender;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.UUID;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class AckVariableId implements Serializable
{
	public static boolean isValid(Path path)
	{
		return Ender.files.getExtension(path)
			.map(filenameExtension::equals)
			.orElse(false);
	}
	
	public static AckVariableId create()
	{
		return new AckVariableId(UUID.randomUUID()); // not realistic to worry about collisions
	}
	
	private static final long serialVersionUID = -1885565949827280904L;
	
	public static final String filenameExtension = "ackv";
	
	@EqualsAndHashCode.Include
	private final UUID uuid;
	
	public Path getPath()
	{
		return AckVariable.storageDirectory.resolve(getUuid().toString() + "." + filenameExtension);
	}
	
	@Override
	public String toString()
	{
		return getUuid().toString();
	}
}
