package endercrypt.bot.ack.modules.variable;


import endercrypt.bot.ack.sendable.Viewable;

import java.io.Serializable;


public interface AckStorable extends Viewable, Serializable
{
}
