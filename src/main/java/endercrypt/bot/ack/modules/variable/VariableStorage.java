package endercrypt.bot.ack.modules.variable;


import endercrypt.bot.ack.modules.variable.exceptions.AckVariableCorruptedException;
import endercrypt.library.commons.interfaces.EqualsCheck;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.function.Function;
import java.util.function.Predicate;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class VariableStorage
{
	public static final int CAPACITY = 10;
	
	private Queue<AckVariable> variables = new PriorityQueue<>(AckVariable.dateComparator);
	
	public Optional<AckVariable> findByName(String name)
	{
		return find(name, AckVariable::getName, String::equalsIgnoreCase);
	}
	
	public Optional<AckVariable> findByUuid(AckVariableId id)
	{
		return find(id, AckVariable::getId, AckVariableId::equals);
	}
	
	private <T> Optional<AckVariable> find(T value, Function<AckVariable, T> getter, EqualsCheck<T> equalsCheck)
	{
		return find(variable -> equalsCheck.isEquals(getter.apply(variable), value));
	}
	
	private synchronized Optional<AckVariable> find(Predicate<AckVariable> predicate)
	{
		return variables.stream()
			.filter(predicate)
			.findFirst();
	}
	
	public synchronized AckVariable get(String name)
	{
		return findByName(name).orElseGet(() -> createExact(name));
	}
	
	public synchronized AckVariable createAvailable(String name)
	{
		return createExact(findAvailableName(name));
	}
	
	public synchronized AckVariable createExact(String name)
	{
		return findByName(name)
			.orElseGet(() -> {
				AckVariableId id = AckVariableId.create();
				AckVariable variable = new AckVariable(id, name);
				add(variable);
				return variable;
			});
	}
	
	private String findAvailableName(String originalName)
	{
		String name = originalName;
		int index = 0;
		while (findByName(name).isPresent())
		{
			index++;
			name = originalName + "@" + index;
		}
		System.out.println(name);
		return name;
	}
	
	public synchronized void add(AckVariable variable)
	{
		if (findByUuid(variable.getId()).isPresent())
		{
			throw new IllegalArgumentException("already contains variable with id: " + variable.getId());
		}
		variables.add(variable);
		while (variables.size() > CAPACITY)
		{
			AckVariable old = variables.remove();
			logger.info("removed +" + old + " (modified: " + old.getModifiedTime() + ")");
		}
	}
	
	public void load(Path path)
	{
		try
		{
			add(AckVariable.read(path));
		}
		catch (AckVariableCorruptedException e)
		{
			logger.error("Failed to read variable", e);
		}
	}
	
	public synchronized List<AckVariable> getAll()
	{
		return List.copyOf(variables);
	}
}
