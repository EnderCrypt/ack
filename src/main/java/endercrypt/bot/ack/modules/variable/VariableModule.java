package endercrypt.bot.ack.modules.variable;


import endercrypt.bot.ack.application.AckApplication;
import endercrypt.bot.ack.application.modules.AckModule;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class VariableModule extends AckModule implements Iterable<AckVariable>
{
	private VariableStorage storage = new VariableStorage();
	
	public VariableModule(AckApplication application)
	{
		super(application);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		Files.createDirectories(AckVariable.storageDirectory);
		try (Stream<Path> stream = Files.list(AckVariable.storageDirectory).filter(AckVariableId::isValid))
		{
			stream.forEach(storage::load);
		}
	}
	
	public synchronized AckVariable push(String name, AckStorable storable)
	{
		AckVariable variable = storage.createAvailable(name);
		variable.setContent(storable);
		logger.info("Pushed variable: " + variable.getName() + " with " + variable.getContentInfo());
		return variable;
	}
	
	public synchronized AckVariable set(String name, AckStorable storable)
	{
		AckVariable variable = storage.get(name);
		variable.setContent(storable);
		logger.info("Set variable: " + variable.getName() + " with " + variable.getContentInfo());
		return variable;
	}
	
	public synchronized boolean delete(String name)
	{
		AckVariable variable = storage.findByName(name).orElse(null);
		if (variable != null)
		{
			variable.clear();
			return true;
		}
		return false;
	}
	
	public synchronized Optional<AckStorable> get(String name)
	{
		return storage.findByName(name).map(AckVariable::getContent);
	}
	
	public synchronized List<AckVariable> getAll()
	{
		return storage.getAll();
	}
	
	public Stream<AckVariable> stream()
	{
		return getAll().stream();
	}
	
	@Override
	public Iterator<AckVariable> iterator()
	{
		return stream().iterator();
	}
}
