package endercrypt.bot.ack.modules.variable;


import endercrypt.bot.ack.modules.variable.exceptions.AckVariableCorruptedException;
import endercrypt.bot.ack.modules.variable.exceptions.AckVariableException;
import endercrypt.bot.ack.sendable.Viewable;
import endercrypt.library.commons.JarInfo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Optional;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;


@Log4j2
@Getter
public class AckVariable implements Viewable, Serializable
{
	public static AckVariable read(Path path) throws AckVariableCorruptedException
	{
		try (ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(new FileInputStream(path.toFile()))))
		{
			return (AckVariable) input.readObject();
		}
		catch (IOException | ClassNotFoundException e1)
		{
			try
			{
				Files.delete(path);
			}
			catch (IOException e2)
			{
				logger.error("Failed to delete " + path, e2);
			}
			throw new AckVariableCorruptedException(path.toString(), e1);
		}
	}
	
	private static final long serialVersionUID = -9101139901643199197L;
	
	public static final Path storageDirectory = JarInfo.workingDirectory.resolve("./var/storage/");
	public static final Comparator<AckVariable> dateComparator = Comparator.comparing(AckVariable::getModifiedTime);
	
	private final AckVariableId id;
	private final String name;
	private LocalDateTime modifiedTime;
	private AckStorable content = null;
	
	public AckVariable(@NonNull AckVariableId id, @NonNull String name)
	{
		this.id = id;
		this.name = name;
		touch();
	}
	
	public synchronized void setContent(AckStorable content)
	{
		this.content = content;
		touch();
	}
	
	public synchronized AckStorable getContent()
	{
		if (isNull())
		{
			throw new AckVariableException(getName() + " is null");
		}
		return content;
	}
	
	public synchronized <T extends AckStorable> T getContent(Class<T> targetClass)
	{
		if (targetClass.isInstance(getContent()))
		{
			return targetClass.cast(getContent());
		}
		throw new AckVariableException(getName() + " is not of type " + targetClass.getSimpleName());
	}
	
	public void clear()
	{
		setContent(null);
	}
	
	public synchronized boolean isNull()
	{
		return content == null;
	}
	
	public String getContentInfo()
	{
		return Optional.ofNullable(getContent())
			.map(AckStorable::getInfo)
			.orElse("null");
	}
	
	@Override
	public String getInfo()
	{
		return getName() + ": " + getContentInfo();
	}
	
	public void touch()
	{
		this.modifiedTime = LocalDateTime.now();
		try
		{
			touchStorageFile();
		}
		catch (IOException e)
		{
			throw new AckVariableException("Failed to write variable: " + getName() + " to " + getId().getPath(), e);
		}
	}
	
	private synchronized void touchStorageFile() throws IOException, FileNotFoundException
	{
		if (isNull())
		{
			Files.deleteIfExists(getId().getPath());
		}
		else
		{
			try (ObjectOutputStream output = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(getId().getPath().toFile()))))
			{
				output.writeObject(this);
			}
		}
	}
}
