package endercrypt.bot.ack.modules.variable.exceptions;

@SuppressWarnings("serial")
public class AckVariableCorruptedException extends Exception
{
	public AckVariableCorruptedException(String message)
	{
		super(message);
	}
	
	public AckVariableCorruptedException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
