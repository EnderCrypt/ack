package endercrypt.bot.ack.modules.variable.exceptions;

@SuppressWarnings("serial")
public class AckVariableException extends RuntimeException
{
	public AckVariableException(String message)
	{
		super(message);
	}
	
	public AckVariableException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
