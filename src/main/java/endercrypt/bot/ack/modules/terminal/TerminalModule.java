package endercrypt.bot.ack.modules.terminal;


import endercrypt.bot.ack.application.AckApplication;
import endercrypt.bot.ack.interaction.message.SystemAckMesssage;
import endercrypt.bot.ack.modules.command.CommandModule;
import endercrypt.library.framework.systems.modules.CoreModuleDependencies;
import endercrypt.library.framework.systems.modules.CoreThreadModule;


public class TerminalModule extends CoreThreadModule<AckApplication>
{
	public TerminalModule(AckApplication application)
	{
		super(application);
	}
	
	@Override
	public CoreModuleDependencies dependencies()
	{
		return super.dependencies()
			.add(CommandModule.class);
	}
	
	@Override
	protected void thread() throws Exception
	{
		while (true)
		{
			SystemAckMesssage message = SystemAckMesssage.poll();
			application.modules().command().execute(message);
		}
	}
}
